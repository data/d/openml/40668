# OpenML dataset: connect-4

https://www.openml.org/d/40668

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: John Tromp  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Connect-4) - 1995  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Connect-4**  
This database contains all legal 8-ply positions in the game of connect-4 in which neither player has won yet, and in which the next move is not forced. Attributes represent board positions on a 6x6 board. The outcome class is the game-theoretical value for the first player (2: win, 1: loss, 0: draw).

### Attribute Information  

The board is numbered like:  
6 . . . . . . .  
5 . . . . . . .  
4 . . . . . . .  
3 . . . . . . .  
2 . . . . . . .  
1 . . . . . . .  
a b c d e f g  

The values represent:  
0: Blank  
1: Taken by Player 1  
2: Taken by Player 2

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40668) of an [OpenML dataset](https://www.openml.org/d/40668). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40668/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40668/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40668/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

